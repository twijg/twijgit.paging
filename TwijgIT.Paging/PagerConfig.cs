namespace TwijgIT.Paging {
  using JetBrains.Annotations;

  [PublicAPI]
  public struct PagerConfig {
    private const int DefaultNumberAtStart = 2;
    private const int DefaultNumberBeforeCurrent = 1;
    private const int DefaultNumberAfterCurrent = 1;
    private const int DefaultNumberAtEnd = 1;
    private const int DefaultFirstPageNumber = 0;
    private const int DefaultMinimumGapSize = 2;

    private int? numberAtStart;
    private int? numberBeforeCurrent;
    private int? numberAfterCurrent;
    private int? numberAtEnd;
    private int? firstPageNumber;
    private int? minimumGapSize;

    public PagerConfig(
      int numberAtStart = DefaultNumberAtStart,
      int numberBeforeCurrent = DefaultNumberBeforeCurrent,
      int numberAfterCurrent = DefaultNumberAfterCurrent,
      int numberAtEnd = DefaultNumberAtEnd,
      int firstPageNumber = DefaultFirstPageNumber,
      int minimumGapSize = DefaultMinimumGapSize) {
      this.numberAtStart = numberAtStart;
      this.numberBeforeCurrent = numberBeforeCurrent;
      this.numberAfterCurrent = numberAfterCurrent;
      this.numberAtEnd = numberAtEnd;
      this.firstPageNumber = firstPageNumber;
      this.minimumGapSize = minimumGapSize;
    }

    public int NumberAtStart => this.numberAtStart ?? DefaultNumberAtStart;

    public int NumberBeforeCurrent => this.numberBeforeCurrent ?? DefaultNumberBeforeCurrent;

    public int NumberAfterCurrent => this.numberAfterCurrent ?? DefaultNumberAfterCurrent;

    public int NumberAtEnd => this.numberAtEnd ?? DefaultNumberAtEnd;

    public int FirstPageNumber => this.firstPageNumber ?? DefaultFirstPageNumber;

    public int MinimumGapSize => this.minimumGapSize ?? DefaultMinimumGapSize;
  }
}
