﻿namespace TwijgIT.Paging {
  using System.Collections.Generic;
  using System.Linq;

  using JetBrains.Annotations;

  [PublicAPI]
  public class Pager {
    private readonly PagerConfig config;

    /// <summary>
    /// Initializes a new instance of the Pager class.
    /// </summary>
    /// <param name="config">The pager config.</param>
    public Pager(PagerConfig config = default) {
      this.config = config;
    }

    /// <summary>
    /// Gets the pages that should be included in the pager.
    /// </summary>
    /// <param name="currentPage">
    /// The number of the current page, using FirstPageNumber from config.
    /// </param>
    /// <param name="pageCount">
    /// The total number of pages.
    /// </param>
    /// <returns>
    /// In order: the page numbers that should be included in the pager and null values for gaps.
    /// </returns>
    /// <remarks>
    /// <list type="bullet">
    /// <item>
    /// <description>
    /// If options conflict (i.e. config.NumberAtStart and config.NumberAfterCurrent), the larger number preceeds.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Gaps MUST NOT appear at start or at end.
    /// </description>
    /// </item>
    /// <item>
    /// <description>
    /// Gaps smaller than config.MinimumGapSize must be replaced by the corresponding page numbers.
    /// </description>
    /// </item>
    /// </list>
    /// </remarks>
    public IEnumerable<int?> Pages(int currentPage, int pageCount) {
      var pageNumberFirst = this.config.FirstPageNumber;
      var pageNumberLast = pageCount + pageNumberFirst - 1;

      // generate the begin, middle and end ranges
      var beginNumbers = Enumerable.Range(pageNumberFirst, this.config.NumberAtStart);
      var middleNumbers = Enumerable.Range(
        currentPage - this.config.NumberBeforeCurrent,
        this.config.NumberAfterCurrent + this.config.NumberBeforeCurrent + 1);
      var endNumbers = Enumerable.Range(pageNumberLast - this.config.NumberAtEnd + 1, this.config.NumberAtEnd);

      var numbers = beginNumbers.Union(middleNumbers)
        .Union(endNumbers)
        .Where(n => n >= pageNumberFirst)
        .Where(n => n <= pageNumberLast)
        .ToArray();

      for (var i = 0; i < numbers.Length; i++) {
        yield return numbers[i];

        // avoid out of bounds
        if (i + 1 >= numbers.Length) {
          continue;
        }

        // determine whether there is a gap
        if (numbers[i + 1] == numbers[i] + 1) {
          continue; // normal sequence
        }

        if (numbers[i + 1] >= numbers[i] + this.config.MinimumGapSize + 1) {
          yield return null; // gap
        }
        else {
          var gapRange = Enumerable.Range(numbers[i] + 1, numbers[i + 1] - numbers[i] - 1);
          foreach (var gapNum in gapRange) {
            yield return gapNum; // fill gap that is too small
          }
        }
      }
    }
  }
}
