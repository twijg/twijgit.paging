namespace TwijgIT.Paging.Test {
  using System.Linq;

  using Xunit;

  public class PagerTests {
    [Fact]
    public void BeginGapTest() {
      var config = new PagerConfig(3, 1, 1, 1, 0, 3);
      var actual = new Pager(config).Pages(6, 10).ToArray();
      int?[] expected = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void BeginOverlapTest() {
      var actual = new Pager().Pages(2, 10).ToArray();
      int?[] expected = { 0, 1, 2, 3, null, 9 };

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void DefaultTest() {
      var actual = new Pager().Pages(5, 10).ToArray();
      int?[] expected = { 0, 1, null, 4, 5, 6, null, 9 };

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void EndGapTest() {
      var config = new PagerConfig(2, 1, 1, 1, 0, 3);
      var actual = new Pager().Pages(6, 10).ToArray();
      int?[] expected = { 0, 1, null, 5, 6, 7, 8, 9 };

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void EndOverlapTest() {
      var actual = new Pager().Pages(9, 10).ToArray();
      int?[] expected = { 0, 1, null, 8, 9 };

      Assert.Equal(expected, actual);
    }

    [Fact]
    public void FirstNumberTest() {
      var config = new PagerConfig(2, 1, 1, 1, 2, 2);
      var actual = new Pager(config).Pages(5, 10).ToArray();
      int?[] expected = { 2, 3, 4, 5, 6, null, 11 };

      Assert.Equal(expected, actual);
    }
  }
}
